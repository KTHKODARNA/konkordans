import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.io.BufferedInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Collections;
import java.io.DataInputStream;
import java.nio.*;
import java.nio.channels.FileChannel;
import java.nio.ByteBuffer;

public class Engin {


	int index = 1;
	long[] prefixArray = new long[28000];
	RandomAccessFile raf;
	BufferedReader br;
	ArrayList<String> interval = new ArrayList<String>();
	int occurrences;
	int wordLength;
	DataOutputStream dos;
	Scanner scanner;
	long startTime;
	

	public Engin(String s) throws IOException {
		startTime = System.currentTimeMillis(); // starta tidtagning
		if(s.equals("init_file_0")) {
			init();
			writeToArrayFile();
			System.out.println("Time: " + (System.currentTimeMillis() - startTime)); // sluta tidtagning
		} else {
			s = s.toLowerCase();
			search(s);
			printOccurrences();
		}
	}

	public void init() throws IOException {
		Process cmdProc = Runtime.getRuntime().exec("/info/adk14/labb1/tokenizer < /info/adk14/labb1/korpus | sort > src/output21.txt");

		br = new BufferedReader(new InputStreamReader(new FileInputStream(new File("src/output21.txt")), "ISO-8859-1"));
		long position = 0;
		String currentLine;
		String currentPrefix = "";
		br.mark(100);
		while(br.readLine() != null) {
			br.reset();
			currentLine = br.readLine();
			String[] line = currentLine.split("\\s+");
			line[0] = getPrefix(line[0]);
			if(!currentPrefix.equals(line[0])) {
				currentPrefix = getPrefix(line[0]);
				insertAddress(position, getIndex(currentPrefix));
			}
			position += currentLine.length() + 1;
			br.mark(100);
		}
		insertAddress(189178528, 27998);
		fillEmptySpaces();
	}

	public void fillEmptySpaces() {
		long toFillWith = 0;
		for(int i = prefixArray.length-1; i>1; i--) {
			if(prefixArray[i] == 0) {
				prefixArray[i] = toFillWith;
			} else {
				toFillWith = prefixArray[i];
			}
		}
	}

	public void writeToArrayFile() throws IOException {
		raf = new RandomAccessFile("src/arrayfile.txt", "rw");
		for(int i=0; i<prefixArray.length; i++) {
			raf.writeLong(prefixArray[i]);
		}
		raf.close();
	}
	public boolean search(String s) throws IOException {
		storeIntervalToArray(s);
		int index = binSearch(s);
		if(index == -1) {
			System.out.println("Ordet " + s + " finns inte i korpusen");
			return false;
		}
		findStringInInterval(s, index);
		System.out.println("\nDet finns " + occurrences + " förekomster av ordet.");
		System.out.println("Det tog: " + (System.currentTimeMillis() - startTime) + "ms.");
		return true;		
	}
	

	/**
	 * Iterates through the interval array and finds 
	 * the input String.
	 * @param s
	 * @throws IOException
	 */

	public void findStringInInterval(String s, int index) throws IOException {
		s = s.toLowerCase();
		occurrences = 0;

		for(int i = index; i < interval.size(); i++) {
			String[] word = interval.get(i).split("\\s+");
			wordLength = word[0].length();
			if(s.equals(word[0])) {
				occurrences++;
			} else {
				return;
			}
		}
		return;
	}

	public int binSearch(String s) throws IOException {
		int low = 0;
		int middle = 0;
		int high = interval.size() - 1;
		String[] line;
		while(low < high) {
			middle = low + ((high - low) / 2);
			line = interval.get(middle).split("\\s+");
			if(s.compareTo(line[0]) > 0) {
				low = middle + 1;
			} else {
				high = middle;
			}
		}
		if(interval.size() == 0) {
			return -1;
		}

		line = interval.get(low).split("\\s+");
		if ((low == high) && (s.compareTo(line[0]) == 0)) {
			return low;
		} else {
			return -1;
		}
	}


	/**
	 * Look up number of occurrences in korpus.
	 * @throws IOException
	 */

	public void lookUpOccurrences(int index) throws IOException {
		byte[] b = new byte[(60 + wordLength)];
		raf = new RandomAccessFile("/info/adk14/labb1/korpus", "r");
		String[] line;
		long printStart;
		for(int i=index; i < index+occurrences; i++) {
			for(int j = 0; j < 60 + wordLength; j++) {
				b = new byte[(60 + wordLength)];
				line = interval.get(i).split("\\s+");
				printStart = Long.parseLong(line[1]) - 30l;
				if(printStart < 0) {
					printStart = 0;
				}
				raf.seek(printStart);
				raf.read(b);
			}
			System.out.println(new String(b, "ISO-8859-1").replaceAll("\n", " "));
		}
		raf.close();
	}

	public void printOccurrences() throws IOException {
		if(occurrences > 25) {
			System.out.print("Vill du skriva ut allt? [y/n]");
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			String input = br.readLine().toLowerCase();
			if(input.equals("y")) {
				lookUpOccurrences(index);
				
			} else {
				return;
			}
		}

		lookUpOccurrences(index);
	}


	/**
	 * Tar en strÃƒÂ¤ng och hittar ett intervall med strÃƒÂ¤ngar dÃƒÂ¤r alla strÃƒÂ¤ngar
	 * har samma prefix.
	 * 
	 * @param s
	 *
	 *	TODO TODO TODO TODO TODO
	 * 
	 * @throws IOException
	 */

	public void storeIntervalToArray(String s) throws IOException {
		long startIndex = 8*getIndex(getPrefix(s));

		long startPos = 0;
		long endPos = 0;
		try {
			boolean foundEnd = false;
			raf = new RandomAccessFile("src/arrayfile.txt", "r");
			raf.seek(startIndex);
			startPos = raf.readLong();
	
			while(!foundEnd) {
				endPos = raf.readLong();
				if(endPos != 0) {
					foundEnd = true;
				}
			}

			raf.close();
			RandomAccessFile raf21 = new RandomAccessFile("src/output21.txt", "r");
			FileInputStream fis = new FileInputStream(raf21.getFD());

			raf21.seek(startPos);

			BufferedReader br = new BufferedReader(new InputStreamReader(fis, "ISO-8859-15"));
			
			long currentPosition = startPos;

			while(currentPosition < endPos) {
				String line = br.readLine();
				
				if(line == null) {
					return;
				}

				currentPosition += line.length();
				interval.add(line);
			}
			raf21.close();
		} catch (java.io.EOFException e) {
		}
	}

	public String readline(long position) throws IOException {
		raf = new RandomAccessFile("src/output21.txt", "r");
		raf.seek(position);
		String res = raf.readLine();
		raf.close();
		return res;
	}

	public String getPrefix(String s) {
		s = s.toLowerCase();
		if (s.length() > 3) {
			return s.substring(0,3);
		} else {
			return s;
		}		
	}

	/*
	 * SÃƒÂ¤tter in long i ett index i arrayn. 
	 */
	public void insertAddress(long address, int index) {
		prefixArray[index] = address;

	}

	public int getIndex(String s) {
		char[] input = new StringBuilder(s).reverse().toString().toCharArray();
		int res = 0;
		for(int exponent = 0; exponent < input.length ; exponent++) {
			int temp = 0;
			switch(input[exponent]) {
			case 229:
				temp = 123 - 96;
				break;
			case 228:
				temp = 124 - 96;
				break;
			case 246:
				temp = 125 - 96;
				break;
			default: temp = input[exponent] - 96;

			}
			res += temp * Math.pow(29,exponent);
		}
		return res;
	}

	public long getLongIndex(String s) {
		char[] input = new StringBuilder(s).reverse().toString().toCharArray();
		long res = 0;
		for(int exponent = 0; exponent < input.length ; exponent++) {
			long temp = 0;
			switch(input[exponent]) {
			case 229:
				temp = 123 - 96;
				break;
			case 228:
				temp = 124 - 96;
				break;
			case 246:
				temp = 125 - 96;
				break;
			default: temp = input[exponent] - 96;

			}
			res += temp * Math.pow(29,exponent);
		}
		return res;
	}

}
